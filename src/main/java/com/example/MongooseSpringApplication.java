package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongooseSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongooseSpringApplication.class, args);
	}
}
